package bodies;

public class TransactionInput {

//"tx_index":"12563028"
//"txIn_index":"123123123"
//"hash":"a3e2bcc9a5f776112497a32b05f4b9e5b2405ed9",
//"value":"100000000",
//"tx_index":"12554260",
//"script":"76a914641ad5051edd97029a003fe9efb29359fcee409d88ac

    private long transactionInputId;
    private String hash;
    private long value;
    private long transactionId;
    private String script;

    public TransactionInput(long transactionInputId, String hash, long value, long transactionId, String script) {
        this.transactionInputId = transactionInputId;
        this.hash = hash;
        this.value = value;
        this.transactionId = transactionId;
        this.script = script;
    }

    public long getTransactionInputId() {
        return transactionInputId;
    }

    public void setTransactionInputId(long transactionInputId) {
        this.transactionInputId = transactionInputId;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }
}
