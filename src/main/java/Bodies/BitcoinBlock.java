package bodies;

import java.util.List;

public class BitcoinBlock {

//"hash":"0000000000000bae09a7a393a8acded75aa67e46cb81f7acaa5ad94f9eacd103",
//"ver":1,
//"prev_block":"00000000000007d0f98d9edca880a6c124e25095712df8952e0439ac7409738a",
//"mrkl_root":"935aa0ed2e29a4b81e0c995c39e06995ecce7ddbebb26ed32d550a72e8200bf5",
//"time":1322131230,
//"bits":437129626,
//"nonce":2964215930,
//"n_tx":22,
//"size":9195,
//"block_index":818044,
//"main_chain":true,
//"height":154595,
//"received_time":1322131301,
//"relayed_by":"108.60.208.156"

    private long blockId;
    private String hash;
    private long version;
    private long prevBlockId;
    private String prevBlock;
    private String merkelRoot;
    private long time;
    private long bits;
    private long nonce;
    private long transactionNumber;
    private long size;
    private boolean mainChain;
    private long height;
    private long receivedTime;
    private String relayedBy;
    private List<Transaction> transactionList;

    public BitcoinBlock(long blockId, String hash, long version, long prevBlockId, String prevBlock, String merkelRoot,
                        long time, long bits, long nonce, long transactionNumber,
                        long size, boolean mainChain, long height, long receivedTime, String relayedBy, List<Transaction> transactionList) {
        this.blockId = blockId;
        this.hash = hash;
        this.version = version;
        this.prevBlockId = prevBlockId;
        this.prevBlock = prevBlock;
        this.merkelRoot = merkelRoot;
        this.time = time;
        this.bits = bits;
        this.nonce = nonce;
        this.transactionNumber = transactionNumber;
        this.size = size;
        this.mainChain = mainChain;
        this.height = height;
        this.receivedTime = receivedTime;
        this.relayedBy = relayedBy;
        this.transactionList = transactionList;
    }

    public long getBlockId() {
        return blockId;
    }

    public void setBlockId(long blockId) {
        this.blockId = blockId;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public long getPrevBlockId() {
        return prevBlockId;
    }

    public void setPrevBlockId(long prevBlockId) {
        this.prevBlockId = prevBlockId;
    }

    public String getPrevBlock() {
        return prevBlock;
    }

    public void setPrevBlock(String prevBlock) {
        this.prevBlock = prevBlock;
    }

    public String getMerkelRoot() {
        return merkelRoot;
    }

    public void setMerkelRoot(String merkelRoot) {
        this.merkelRoot = merkelRoot;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getBits() {
        return bits;
    }

    public void setBits(long bits) {
        this.bits = bits;
    }

    public long getNonce() {
        return nonce;
    }

    public void setNonce(long nonce) {
        this.nonce = nonce;
    }

    public long getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(long transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public boolean isMainChain() {
        return mainChain;
    }

    public void setMainChain(boolean mainChain) {
        this.mainChain = mainChain;
    }

    public long getHeight() {
        return height;
    }

    public void setHeight(long height) {
        this.height = height;
    }

    public long getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(long receivedTime) {
        this.receivedTime = receivedTime;
    }

    public String getRelayedBy() {
        return relayedBy;
    }

    public void setRelayedBy(String relayedBy) {
        this.relayedBy = relayedBy;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }
}
