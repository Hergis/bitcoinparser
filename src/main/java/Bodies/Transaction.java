package bodies;

import java.util.List;

public class Transaction {

//"hash":"b6f6991d03df0e2e04dafffcd6bc418aac66049e2cd74b80f14ac86db1e3f0da",
//"ver":1,
//"vin_sz":1,
//"vout_sz":2,
//"lock_time":"Unavailable",
//"size":258,
//"relayed_by":"64.179.201.80",
//"block_height, 12200,
//"tx_index":"12563028",

    private String transactionHash;
    private long version;
    private long numberOfInputs;
    private long numberOfOutputs;
    private long lockTime;
    private long size;
    private String relayedBy;
    private long blockHeight;
    private long transactionIndex;
    private List<TransactionInput> transactionInputList;
    private List<TransactionOutput> transactionOutputList;


    public Transaction(String transactionHash, long version, long numberOfInputs, long numberOfOutputs, long lockTime,
                       long size, String relayedBy, long blockHeight, long transactionIndex,
                       List<TransactionInput> transactionInputList, List<TransactionOutput> transactionOutputList) {
        this.transactionHash = transactionHash;
        this.version = version;
        this.numberOfInputs = numberOfInputs;
        this.numberOfOutputs = numberOfOutputs;
        this.lockTime = lockTime;
        this.size = size;
        this.relayedBy = relayedBy;
        this.blockHeight = blockHeight;
        this.transactionIndex = transactionIndex;
        this.transactionInputList = transactionInputList;
        this.transactionOutputList = transactionOutputList;
    }

    public String getTransactionHash() {
        return transactionHash;
    }

    public void setTransactionHash(String transactionHash) {
        this.transactionHash = transactionHash;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public long getNumberOfInputs() {
        return numberOfInputs;
    }

    public void setNumberOfInputs(long numberOfInputs) {
        this.numberOfInputs = numberOfInputs;
    }

    public long getNumberOfOutputs() {
        return numberOfOutputs;
    }

    public void setNumberOfOutputs(long numberOfOutputs) {
        this.numberOfOutputs = numberOfOutputs;
    }

    public long getLockTime() {
        return lockTime;
    }

    public void setLockTime(long lockTime) {
        this.lockTime = lockTime;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getRelayedBy() {
        return relayedBy;
    }

    public void setRelayedBy(String relayedBy) {
        this.relayedBy = relayedBy;
    }

    public long getBlockHeight() {
        return blockHeight;
    }

    public void setBlockHeight(long blockHeight) {
        this.blockHeight = blockHeight;
    }

    public long getTransactionIndex() {
        return transactionIndex;
    }

    public void setTransactionIndex(long transactionIndex) {
        this.transactionIndex = transactionIndex;
    }

    public List<TransactionInput> getTransactionInputList() {
        return transactionInputList;
    }

    public void setTransactionInputList(List<TransactionInput> transactionInputList) {
        this.transactionInputList = transactionInputList;
    }

    public List<TransactionOutput> getTransactionOutputList() {
        return transactionOutputList;
    }

    public void setTransactionOutputList(List<TransactionOutput> transactionOutputList) {
        this.transactionOutputList = transactionOutputList;
    }
}
