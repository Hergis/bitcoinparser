package bodies;

public class TransactionOutput {
//"value":"98000000",
//"hash":"29d6a3540acfa0a950bef2bfdc75cd51c24390fd",
//"script":"76a914641ad5051edd97029a003fe9efb29359fcee409d88ac"

    private long transactionOutputIndex;
    private long value;
    private String hash;
    private String script;


    public TransactionOutput(long transactionOutputIndex, long value, String hash, String script) {
        this.transactionOutputIndex = transactionOutputIndex;
        this.value = value;
        this.hash = hash;
        this.script = script;
    }

    public long getTransactionOutputIndex() {
        return transactionOutputIndex;
    }

    public void setTransactionOutputIndex(long transactionOutputIndex) {
        this.transactionOutputIndex = transactionOutputIndex;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }
}
