import bodies.BitcoinBlock;
import bodies.Transaction;
import repository.PostgresqlDB;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BitcoinParser {

    private static DataSource dataSource;

    public static void main(String[] args) throws Exception {

        List<BitcoinBlock> bitcoinBlockList = new ArrayList<BitcoinBlock>();
        dataSource = PostgresqlDB.getDataSource();

        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();
        String[] userInputs = input.split(",");

        for (String tempInp : userInputs) {
            Long tempLong = Long.parseLong(tempInp);
            for (BitcoinBlock block : bitcoinBlockList) {
                if (tempLong.equals(block.getBlockId())) {
                    for (Transaction transaction : block.getTransactionList()) {
                        PostgresqlDB.saveTransaction(transaction);
                    }
                }
            }
        }
    }
}
